const form = document.getElementById('registration-form');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');
const emailError = document.getElementById('email-error');
const passwordError = document.getElementById('password-error');
const successMessage = document.getElementById('success-message');

form.addEventListener('submit', function (e) {
    e.preventDefault();

    // Reset error messages
   
    emailError.textContent = '';
    passwordError.textContent = '';
    

   

    // Validate Email
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(emailInput.value)) {
        emailError.textContent = 'Invalid email address';
        emailInput.focus();
        return;
    }

    // Validate Password (minimum 8 characters)
    if (passwordInput.value.length < 8) {
        passwordError.textContent = 'Password must be at least 8 characters';
        passwordInput.focus();
        return;
    }

   
    // If all validations pass, show a success message
    successMessage.textContent = 'Registration successful!';
});