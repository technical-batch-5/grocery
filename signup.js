const form = document.getElementById('registration-form');
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');
const addressInput = document.getElementById('address');
const nameError = document.getElementById('name-error');
const emailError = document.getElementById('email-error');
const passwordError = document.getElementById('password-error');
const addressError = document.getElementById('address-error');
const successMessage = document.getElementById('success-message');

form.addEventListener('submit', function (e) {
    e.preventDefault();

    // Reset error messages
    nameError.textContent = '';
    emailError.textContent = '';
    passwordError.textContent = '';
    addressError.textContent = '';

    // Validate Name
    if (nameInput.value.trim() === '') {
        nameError.textContent = 'Name is required';
        nameInput.focus();
        return;
    }

    // Validate Email
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(emailInput.value)) {
        emailError.textContent = 'Invalid email address';
        emailInput.focus();
        return;
    }

    // Validate Password (minimum 8 characters)
    if (passwordInput.value.length < 8) {
        passwordError.textContent = 'Password must be at least 8 characters';
        passwordInput.focus();
        return;
    }

    // Validate Address
    if (addressInput.value.trim() === '') {
        addressError.textContent = 'Address is required';
        addressInput.focus();
        return;
    }

    // If all validations pass, show a success message
    successMessage.textContent = 'Registration successful!';
});